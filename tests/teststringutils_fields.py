#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from hoist.stringutils import fields

class TestFields(unittest.TestCase):
    def test_empty(self):
        self.assertEqual(len(fields('')), 0)

    def test_bland(self):
        self.assertEqual(len(fields('hello world')), 0)

    def test_unnamed(self):
        self.assertEqual(len(fields('hello %s %s')), 0)

    def test_expected(self):
        self.assertEqual(set(fields('%(hello)s %(world)s')),
            set(['hello', 'world']))

    def test_duplicated(self):
        self.assertEqual(fields('%(hello)s %(hello)s'), ['hello'])

    def test_mixed(self):
        self.assertEqual(fields('%(hello)s %s'), ['hello'])

    def test_unicode(self):
        self.assertEqual(fields(u'%(↑hello)s world'), [u'↑hello'])

if __name__ == '__main__':
    unittest.main()
