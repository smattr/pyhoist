import unittest
from hoist.decorate import null

class TestNull(unittest.TestCase):
    def test_null(self):
        @null()
        def foo(x):
            return x + 1

        for i in xrange(100):
            self.assertEqual(foo(i), i + 1)

if __name__ == '__main__':
    unittest.main()
