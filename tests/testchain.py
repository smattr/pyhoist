import unittest
from hoist.decorate import chain, Decorator

class SetArray(Decorator):
    def __init__(self, target):
        self.target = target

    def wrapped(self, f, *args, **kwargs):
        self.target[0] = 1
        return f(*args, **kwargs)

class TestChain(unittest.TestCase):
    def test_no_chain(self):
        x = [0]

        @SetArray(x)
        def foo():
            pass

        self.assertEqual(x[0], 0)

        foo()

        self.assertEqual(x[0], 1)

    def test_one(self):
        x = [0]

        @chain(SetArray(x))
        def foo():
            pass

        self.assertEqual(x[0], 0)

        foo()

        self.assertEqual(x[0], 1)

    def test_multiple(self):
        x = [0]
        y = [2]
        z = [3]

        @chain(SetArray(x), SetArray(y), SetArray(z))
        def foo():
            pass

        self.assertEqual(x[0], 0)
        self.assertEqual(y[0], 2)
        self.assertEqual(z[0], 3)

        foo()

        self.assertEqual(x[0], 1)
        self.assertEqual(y[0], 1)
        self.assertEqual(z[0], 1)

if __name__ == '__main__':
    unittest.main()
