import unittest
from hoist.memoize import memoize_function, memoize_method

# Provide a function that relies on global state.
state = None
def impure():
    global state
    state += 1
    return state

# Deliberately (incorrectly) memoize this function so we can detect whether the
# memoizer is working.
@memoize_function
def impure_memoized():
    return impure()

class TestMemoizeFunction(unittest.TestCase):
    def test_validate_assumptions(self):
        global state
        state = None

        with self.assertRaises(TypeError):
            impure()

        state = 0
        self.assertEquals(impure(), 1)
        self.assertEquals(impure(), 2)

    def test_memoize_function(self):
        global state
        state = 0

        # Call the function normally.
        self.assertEqual(impure(), 1)

        # The first memoized call should invoke the underlying function, even
        # though we've called it already, because the memoized wrapper has not
        # been called yet.
        self.assertEqual(impure_memoized(), 2)

        # Now we should just get the previous value.
        self.assertEqual(impure_memoized(), 2)
        self.assertEqual(impure_memoized(), 2)

        # Calling the underlying function should still actually execute it.
        self.assertEqual(impure(), 3)
        self.assertEqual(impure(), 4)

        # The memoized version should have still retained the cached value.
        self.assertEqual(impure_memoized(), 2)
        self.assertEqual(impure_memoized(), 2)

# Replicate the same for class methods.
class MyObject(object):
    def __init__(self):
        self.value = 0

    def impure(self):
        self.value += 1
        return self.value

    @memoize_method
    def impure_memoized(self):
        return self.impure()

class TestMemoizeMethod(unittest.TestCase):
    def test_memoize_method(self):
        o = MyObject()

        # Call the function normally.
        self.assertEqual(o.impure(), 1)

        # The first memoized call should invoke the underlying function, even
        # though we've called it already, because the memoized wrapper has not
        # been called yet.
        self.assertEqual(o.impure_memoized(), 2)

        # Now we should just get the previous value.
        self.assertEqual(o.impure_memoized(), 2)
        self.assertEqual(o.impure_memoized(), 2)

        # Calling the underlying function should still actually execute it.
        self.assertEqual(o.impure(), 3)
        self.assertEqual(o.impure(), 4)

        # The memoized version should have still retained the cached value.
        self.assertEqual(o.impure_memoized(), 2)
        self.assertEqual(o.impure_memoized(), 2)

if __name__ == '__main__':
    unittest.main()
