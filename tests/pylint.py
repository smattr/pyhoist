#!/usr/bin/env python

import os, re, subprocess, sys

ME = os.path.abspath(__file__)

hoist = os.path.abspath(os.path.join(os.path.dirname(ME), '..', 'hoist'))

regex = re.compile(r'^.*\.py$')

ret = 0
for root, _, files in os.walk(hoist):
    for f in files:
        if regex.match(f) is None:
            continue
        path = os.path.join(hoist, root, f)
        print 'testing %s... ' % f,
        sys.stdout.flush()
        sys.stderr.flush()
        ret |= subprocess.call(['pylint', '--errors-only', path])

sys.exit(ret)
