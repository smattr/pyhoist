import unittest
from copy import deepcopy
from hoist.moreitertools import consume

class SimpleObject(object):
    def __init__(self, value):
        self.value = value

    def increment(self):
        self.value += 1

class TestConsume(unittest.TestCase):
    def test_base_semantics(self):
        '''
        Validate the semantics of list and generator comprehensions we assume.
        '''
        init = [SimpleObject(0), SimpleObject(1), SimpleObject(2),
            SimpleObject(3)]
        for index, x in enumerate(init):
            self.assertEqual(x.value, index)

        init2 = deepcopy(init)
        [x.increment() for x in init2]
        # This should have affected the elements of `init2` because a list
        # should have been constructed and discarded.
        for index, x in enumerate(init2):
            self.assertEqual(x.value, index + 1)

        init2 = deepcopy(init)
        (x.increment() for x in init2)
        # Nothing should have happened this time around because a generator
        # comprehension is only evaluated as it is consumed.
        for index, x in enumerate(init2):
            self.assertEqual(x.value, index)

        init2 = deepcopy(init)
        generator = (x.increment() for x in init2)
        # Consume the generator.
        while True:
            try:
                generator.next()
            except StopIteration:
                break
        # This should have affected the elements.
        for index, x in enumerate(init2):
            self.assertEqual(x.value, index + 1)

    def test_basic(self):
        init = [SimpleObject(0), SimpleObject(1), SimpleObject(2),
            SimpleObject(3)]
        consume(x.increment() for x in init)
        for index, x in enumerate(init):
            self.assertEqual(x.value, index + 1)

    def test_empty(self):
        consume(x for x in [])

if __name__ == '__main__':
    unittest.main()
