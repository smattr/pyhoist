import unittest
from types import NoneType
from hoist import typed, returns

class TestTyped(unittest.TestCase):
    def test_simple_return(self):
        @returns(int)
        def foo():
            return 42

        self.assertEqual(foo(), 42)

        with self.assertRaises(TypeError):
            foo('hello')

    def test_simple_typed(self):
        @typed(int)
        def foo(x):
            pass

        self.assertEqual(foo(42), None)

        with self.assertRaises(TypeError):
            foo('hello')

    def test_multiple_return(self):
        @returns((int, str))
        def foo(x):
            if x == 0:
                return 42
            elif x == 1:
                return 'hello'
            return []

        self.assertEqual(foo(0), 42)

        self.assertEqual(foo(1), 'hello')

        with self.assertRaises(TypeError):
            foo(2)

    def test_none_return(self):
        @returns(NoneType)
        def foo():
            return None

        self.assertEqual(foo(), None)

    def test_multiple_typed(self):
        @typed((int, str))
        def foo(x):
            pass

        self.assertEqual(foo(42), None)

        self.assertEqual(foo('hello'), None)

        with self.assertRaises(TypeError):
            foo([])

    def test_multiple_typed2(self):
        @typed(int, str, bool)
        def foo(x, y, z):
            pass

        self.assertEqual(foo(42, 'hello', True), None)

        with self.assertRaises(TypeError):
            foo('hello', 'hello', True)

        with self.assertRaises(TypeError):
            foo(42, 42, True)

        with self.assertRaises(TypeError):
            foo(42, 'hello', 'hello')

    def test_combination(self):
        @typed(int, (str, bool))
        @returns((int, NoneType))
        def foo(x, y):
            if x == 0:
                return 'hello'
            elif x == 1:
                return 42
            return None

        self.assertEqual(foo(1, 'hello'), 42)

        self.assertEqual(foo(1, True), 42)

        with self.assertRaises(TypeError):
            foo(42, 42)

        with self.assertRaises(TypeError):
            foo(0, True)

    def test_with_default(self):
        @typed(int, (int, str))
        def foo(x, y=32):
            pass

        self.assertEqual(foo(42, 42), None)

        self.assertEqual(foo(42, 'hello'), None)

        self.assertEqual(foo(42), None)

        with self.assertRaises(TypeError):
            foo(42, [])

    def test_naming_positional(self):
        @typed(int, str)
        def foo(x, y):
            pass

        with self.assertRaises(TypeError):
            foo(x=[], y=[])

        with self.assertRaises(TypeError):
            foo(y=[], x=42)

    def test_complex(self):
        @typed(int, int, int, text=str)
        def foo(x, y, z, **kwargs):
            pass

        foo(1, 2, 3)

        foo(1, 2, 3, text='hello')

        with self.assertRaises(TypeError):
            foo(1, 2, 'hello')

        foo(1, 2, 3, unrelated=[])

        with self.assertRaises(TypeError):
            foo(1, 2, 3, text=[])

    def test_any_arg1(self):
        @typed(any)
        def foo(x):
            pass

        foo(1)

        foo('hello')

        class Foo(object):
            pass
        foo(Foo())

    def test_any_arg(self):
        @typed(int, any)
        def foo(x, y):
            pass

        foo(1, 2)

        foo(1, 'hello')

        with self.assertRaises(TypeError):
            foo('hello', 1)

    def test_return_any(self):
        @returns(any)
        def foo(x):
            if x:
                return 1
            return 'hello'

        foo(True)

        foo(False)

if __name__ == '__main__':
    unittest.main()
