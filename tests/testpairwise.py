import unittest
from hoist.moreitertools import pairwise

class TestPairwise(unittest.TestCase):
    def test_empty(self):
        empty = pairwise([])
        with self.assertRaises(StopIteration):
            empty.next()

    def test_odd(self):
        invalid = pairwise([1])
        with self.assertRaises(StopIteration):
            invalid.next()

    def test_singleton(self):
        sing = pairwise([1, 2])
        self.assertEqual(sing.next(), (1, 2))
        with self.assertRaises(StopIteration):
            sing.next()

    def test_multiple(self):
        multiple = pairwise([1, 2, 3, 4, 5, 6])
        self.assertEqual(multiple.next(), (1, 2))
        self.assertEqual(multiple.next(), (3, 4))
        self.assertEqual(multiple.next(), (5, 6))
        with self.assertRaises(StopIteration):
            multiple.next()

    def test_trailer(self):
        odd = pairwise([1, 2, 3, 4, 5])
        self.assertEqual(odd.next(), (1, 2))
        self.assertEqual(odd.next(), (3, 4))
        with self.assertRaises(StopIteration):
            odd.next()

if __name__ == '__main__':
    unittest.main()
