#!/usr/bin/env python

import os, sys, unittest

ME = os.path.abspath(__file__)

# Make hoist importable
sys.path.append(os.path.join(os.path.dirname(ME), '..'))

from testchain import TestChain
from testconsume import TestConsume
from testmemoize import TestMemoizeFunction, TestMemoizeMethod
from testpairwise import TestPairwise
from teststringutils_fields import TestFields
from testtyped import TestTyped
from testundecorate import TestUndecorate

if __name__ == '__main__':
    unittest.main()
