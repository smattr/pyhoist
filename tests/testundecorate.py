import unittest
from functools import partial
from hoist.decorate import undecorate

class TestUndecorate(unittest.TestCase):
    def test_no_wrap(self):
        def foo():
            pass

        self.assertEqual(undecorate(foo), foo)

    def test_partial(self):
        def foo(x):
            pass

        f = partial(foo, 42)

        self.assertNotEqual(foo, f)

        self.assertEqual(undecorate(f), foo)

if __name__ == '__main__':
    unittest.main()
