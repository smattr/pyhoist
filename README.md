# Hoist

Generic functionality missing from the Python standard library. You can think
of this like C++'s Boost or Haskell's MissingH.
