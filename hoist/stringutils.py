from collections import defaultdict
from re import sub

def fields(s):
    '''
    Return the format fields present in a string. E.g.
      string_fields('hello %(world)s foo %(bar)s') == ['world', 'bar']
    '''
    # First strip any unnamed fields that will cause TypeError
    s = sub(r'%[^\(]', '', s)

    # A sink to capture any formatting fields
    d = defaultdict(str)

    # Extract the fields
    s % d # value deliberately discarded

    return d.keys()
