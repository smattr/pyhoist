import decorate, moreitertools
from .memoize import memoize_function, memoize_method
from . import stringutils
from .typed import returns, typed
