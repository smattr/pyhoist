'''
Decorators for runtime type-checking of functions.

Sample usage:

    @typed(int, int)
    def adder(x, y):
        return x + y

    @returns(int)
    def i_return_an_int():
        returns 42

    @typed((int, float))
    def accepts_multiple(x):
        return x + 1

    @typed(int, str, (list, types.NoneType))
    @returns(dict)
    def more_complicated(x, y, z=None):
        return { x:y, 'extra':z }
'''

from .decorate import Decorator, undecorate
from functools import partial
from inspect import getargspec

def throw_error(func, name, arg):
    raise TypeError('incorrect type %s for %s of %s' %
        (type(arg).__name__, name, func))

def type_check(func, name, arg, types):

    if types == any:
        # No type constraint for this argument
        return

    elif isinstance(types, type):
        # We have a single type
        if not isinstance(arg, types):
            throw_error(func, name, arg)

    else:
        # We have an iterable of types
        for t in types:
            if isinstance(arg, t):
                break
        else:
            throw_error(func, name, arg)

class typed(Decorator):
    def __init__(self, *args, **kwargs):
        # Save the type signature of the function to use later
        self.args = args
        self.kwargs = kwargs

    def wrapped(self, f, *args, **kwargs):

        # Check the positional arguments
        for i, (arg, types) in enumerate(zip(args, self.args)):
            type_check(f.__name__, 'argument %s' % (i + 1), arg, types)

        argnames = getargspec(undecorate(f)).args

        # Check the named arguments
        for k, v in kwargs.items():
            if k in argnames:
                # This was actually a positional argument, but the caller named
                # it
                index = argnames.index(k)
                try:
                    types = self.args[index]
                except IndexError:
                    # No type given for this argument
                    continue
            else:
                try:
                    types = self.kwargs[k]
                except KeyError:
                    # No type given for this argument
                    continue
            type_check(f.__name__, k, v, types)

        # Call the actual function
        return f(*args, **kwargs)

class returns(Decorator):
    def __init__(self, types):
        self.types = types

    def wrapped(self, f, *args, **kwargs):

        # Call the actual function
        result = f(*args, **kwargs)

        # Type check the result
        type_check(f.__name__, 'return value', result, self.types)

        return result
