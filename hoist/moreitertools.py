from collections import deque
from itertools import islice, izip

def consume(iterator, n=None):
    "Advance the iterator n-steps ahead. If n is none, consume entirely."
    # Use functions that consume iterators at C speed.
    if n is None:
        # feed the entire iterator into a zero-length deque
        deque(iterator, maxlen=0)
    else:
        # advance to the empty slice starting at position n
        next(islice(iterator, n, n), None)

def pairwise(iterable):
    '''Pairwise iteration on a Python collection, courtesy of the itertools
    recipes.'''
    it = iter(iterable)
    return izip(it, it)
