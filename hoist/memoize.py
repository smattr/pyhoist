from functools import partial

class memoize_function(object):
    '''
    Decorator. Cache a function's return value each time it is called. If
    called later with the same arguments, the cached value is returned (not
    reevaluated).

    https://wiki.python.org/moin/PythonDecoratorLibrary#Memoize.
    '''

    def __init__(self, func):
        self.func = func
        self.cache = {}

    def __call__(self, *args, **kwargs):
        key = (args, frozenset(kwargs.items()))
        try:
            res = self.cache[key]
        except KeyError:
            res = self.cache[key] = self.func(*args, **kwargs)
        return res

    def __repr__(self):
        '''Return the function's docstring.'''
        return self.func.__doc__

class memoize_method(object):
    '''
    Decorator for caching an instance method.

    http://code.activestate.com/recipes/577452-a-memoize-decorator-for-instance-methods/
    '''

    def __init__(self, func):
        self.func = func

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self.func
        return partial(self, obj)

    def __call__(self, *args, **kwargs):
        obj = args[0]
        try:
            cache = obj.__cache
        except AttributeError:
            cache = obj.__cache = {}
        key = (self.func, args[1:], frozenset(kwargs.items()))
        try:
            res = cache[key]
        except KeyError:
            res = cache[key] = self.func(*args, **kwargs)
        return res
