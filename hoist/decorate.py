'''
Helpers for writing function decorators.
'''

from abc import ABCMeta, abstractmethod
from functools import partial

class Decorator(object):
    '''
    Generic decorator that accepts arguments.

    Wrapping a function with a decorator can be an awkward process and
    mimicking the original function's characteristics such that more advanced
    Python functionality still works as expected can be complicated. This class
    does the work for you and you simply have to implement the wrapper function
    itself (and optionally `__init__`).
    '''
    __metaclass__ = ABCMeta

    @abstractmethod
    def wrapped(self, f, *args, **kwargs):
        '''
        A wrapped invocation of the original function. When your decorator is
        applied, this function will be invoked with the original function and
        its arguments as arguments to this function.
        '''
        raise NotImplementedError

    def __call__(self, f):
        '''
        Wrap the original function to make this class usable as a decorator.
        '''
        fn = partial(self.wrapped, f)
        fn.__name__ = f.__name__
        fn.__dict__.update(f.__dict__)
        fn.__doc__ = f.__doc__
        return fn

def undecorate(fn):
    '''
    Retrieve the original function from within a decorator.

    This is sometimes necessary when you need to perform operations like
    `inspect.getargspec` on the function. Your intuition may lead you to
    believe that you can simply operate directly on the instance of the
    function that is passed to `Decorator.wrapped`. The problem is that the
    function you are decorating may have multiple decorations. Unless you are
    the first decorator, the function you are passed will actually be a
    `functools.partial`, on which you cannot call `inspect.getargspec`. The
    solution is to unwrap the curried instance as this function does.
    '''
    original = fn
    while isinstance(original, partial):
        original = original.func
    return original

class chain(Decorator):
    '''
    Decorator for chaining other decorators together.

    The purpose of this is to perform operations like `functools.partial` with
    decorators.
    '''
    def __init__(self, *decorators):
        self.decorators = decorators

    def wrapped(self, f, *args, **kwargs):
        fn = reduce(lambda x, y: y(x), self.decorators, f)
        return fn(*args, **kwargs)

class null(Decorator):
    '''
    A decorator that does nothing.
    '''
    def wrapped(self, f, *args, **kwargs):
        return f(*args, **kwargs)
